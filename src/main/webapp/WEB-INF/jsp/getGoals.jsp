<%--
  Created by IntelliJ IDEA.
  User: megryan
  Date: 11/12/17
  Time: 1:10 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Goals</title>
</head>
<body>

<table>
    <tr>
        <th>Goal.ID</th><th>Goal.Minutes</th>
    </tr>
    <c:forEach items="${goals}" var="goal">
        <tr>
            <td>${goal.id}</td>
            <td>${goal.minutes}</td>
            <td>
                <table>
                    <tr>
                        <th> Exercise.ID</th>&nbsp;<th>Exercise.Minutes</th>&nbsp;<th>Exercise.Activity</th>
                    </tr>
                    <tr>
                        <c:forEach items="${goal.exercises}" var="exercise">
                            <tr>
                                <td>${exercise.id}</td>&nbsp;
                                <td>${exercise.minutes}</td>&nbsp;
                                <td>${exercise.activity}</td>
                            </tr>
                        </c:forEach>
                    </tr>
                </table>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
