package com.pluralsight.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

@Entity
@Table(name="exercises")
public class Exercise {

	@Id
	@GeneratedValue
	private Long id;

	@ManyToOne
	private Goal goal;
	
	@Range(min = 1, max = 120)
	private int minutes;
	
	@NotNull
	private String activity;

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Goal getGoal() {
		return goal;
	}

	public void setGoal(Goal goal) {
		this.goal = goal;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {

		this.minutes = minutes;
	}

}
