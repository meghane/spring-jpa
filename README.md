spring_mvc_base
===============

Base project used for Pluralsight Spring JPA and Spring Security courses among others.

http://pluralsight.com/training/Courses/TableOfContents/maven-fundamentals

http://pluralsight.com/training/Courses/TableOfContents/springmvc-intro


### Database: MySql ###
+ Install: `sudo apt-get install mysql-server`
+ Create Schema: `CREATE SCHEMA fitnessTracker;`

### Config ###

#### JpaContext.xml ####
+ `src/main/resources`
+ Picked up by `src/main/webapp/web.xml`

#### JpaContext web.xml Example ####
        
        
    ```   
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:/jpaContext.xml</param-value>
    </context-param>
    	
    <listener>
        <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
    </listener>
     
    ```   
    
    
#### JpaContext.xml entityManagerFactory jpaPropertyMap ####
    
    
    ```   
    <property name="jpaPropertyMap">
        <map>
            <entry key="hibernate.dialect" value="org.hibernate.dialect.MySQL5InnoDBDialect"/>
            <entry key="hibernate.hbm2ddl.auto" value="create"/>
            <entry key="hibernate.format_sql" value="true" />
        </map>
    </property> 
     
    ```   
    
#### FetchType.LAZY And Allowing JPA Sessions to Reach Controller - web.xml ####
        
        
    ```   
    <filter>
        <filter-name>Spring OpenEntityManagerInViewFilter</filter-name>
        <filter-class>org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter</filter-class>
    </filter>
    
    <filter-mapping>
        <filter-name>Spring OpenEntityManagerInViewFilter</filter-name>
        <url-pattern>/*</url-pattern>
    </filter-mapping>
     
    ```   

hibernate.hbm2ddl.auto value="create || create-drop || update || validate || none"

+ create - if schema is defined, JPA will take entities and create them in db 
+ create-drop - JPA will create db on app startup, drops tables with JPA mappings on app shutdown
+ update - JPA updates fields which have changed, but doesnt delete anything already defined
+ validate - prod config. Validates JPA entities aligns with db structure

#### Other Definitions ####
+ PersistenceUnit - defines the set of all classes which are related or grouped by the application, which must be colocated in their mapping to a single db
+ JpaVendorAdaptor (in jpaContext.xml) references the entityManager impl

#### Repository ####
+ `@PersistenceContext` injects Entity Manager in our code
+ Db Interaction occurs (CRUD)
+ Read about [CRUDRepository](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.core-concepts)



